﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Veritask.Tests
{
    using Veritask.Model;

    [TestClass]
    public class TariffStrategyTests
    {
        [TestMethod]
        public void BasicTariffCalculateAnnualCost_WhenCalled_ReturnskWhCost()
        {
            // arrange
            var tariff = new BasicTariffStrategy();
            double consumption = 5000.0;

            // act
            var result = tariff.CalculateAnnualCost(consumption);

            // assert
            Assert.AreEqual(60 + (consumption * 0.22), result);
        }

        [TestMethod]
        public void BasicTariffCalculateAnnualCost_WhenNegativeConsumptionGiven_ArgumentOutOfRangeExceptionThrown()
        {
            // arrange
            var tariff = new BasicTariffStrategy();
            
            // act'n'assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => tariff.CalculateAnnualCost(-1.0));
        }

        [TestMethod]
        public void PackagedTariffCalculateAnnualCost_WhenConsLesserThanThreshold_ReturnsFixedCost()
        {
            // arrange
            var tariff = new PackagedTariffStrategy();
            double consumption = 3999.0;

            // act
            var result = tariff.CalculateAnnualCost(consumption);

            // assert
            Assert.AreEqual(800.0, result);
        }

        [TestMethod]
        public void PackagedTariffCalculateAnnualCost_WhenConsBiggerThanThreshold_ReturnsWithkWhCost()
        {
            // arrange
            var tariff = new PackagedTariffStrategy();
            double consumption = 4001.0;

            // act
            var result = tariff.CalculateAnnualCost(consumption);

            // assert
            Assert.AreEqual(800.0 + (0.3 * (consumption - 4000)), result);
        }

        [TestMethod]
        public void PackagedTariffCalculateAnnualCost_WhenNegativeConsumptionGiven_ArgumentOutOfRangeExceptionThrown()
        {
            // arrange
            var tariff = new PackagedTariffStrategy();

            // act'n'assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => tariff.CalculateAnnualCost(-1.0));
        }
    }
}
