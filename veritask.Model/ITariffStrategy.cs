﻿namespace Veritask.Model
{
    public interface ITariffStrategy
    {
        double CalculateAnnualCost(double consumption);
    }
}
