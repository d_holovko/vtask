﻿namespace Veritask.Model
{
    using System.Collections.Generic;
    using System.Linq;

    public class ProductsList
    {
        private readonly List<Product> products = new List<Product>();

        public void AddProduct(Product product)
        {
            products.Add(product);
        }

        public IEnumerable<Product> GetSortedProducts(double consumption)
        {
            foreach (var product in products)
            {
                product.CalculateAnnualCost(consumption);
            }

            return products.OrderBy(x => x.AnnualCost);
        }
    }
}
