﻿namespace Veritask.Model
{
    using System;

    public class PackagedTariffStrategy : ITariffStrategy
    {
        private const double EuroPricePerYear = 800.0;

        private const double EuroPricePerkWh = 0.3;

        private const double ConsumptionThreshold = 4000;

        public double CalculateAnnualCost(double consumption)
        {
            if (consumption < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(consumption), "Argument can't be less than zero.");
            }

            return consumption < ConsumptionThreshold ? EuroPricePerYear : EuroPricePerYear + (EuroPricePerkWh * (consumption - ConsumptionThreshold));
        }
    }
}
