﻿namespace Veritask.Model
{
    public class Product
    {
        private ITariffStrategy _tariffStrategy;

        private ProductTariff _tariff;

        private double _annualCost;

        public ProductTariff Tariff
        {
            get => _tariff;
            set
            {
                switch (value)
                {
                    case ProductTariff.Basic:
                        _tariffStrategy = new BasicTariffStrategy();
                        break;
                    case ProductTariff.Packaged:
                        _tariffStrategy = new PackagedTariffStrategy();
                        break;
                }
            }

        }

        public Product(ProductTariff tariff)
        {
            _tariff = tariff;
            switch (tariff)
            {
                case ProductTariff.Basic:
                    _tariffStrategy = new BasicTariffStrategy();
                    break;
                case ProductTariff.Packaged:
                    _tariffStrategy = new PackagedTariffStrategy();
                    break;
            }
        }

        public double AnnualCost => _annualCost;

        public double CalculateAnnualCost(double consumption)
        {
            return _annualCost = _tariffStrategy.CalculateAnnualCost(consumption);
        }
    }

    public enum ProductTariff
    {
        Basic,
        Packaged
    }
}
