﻿namespace Veritask.Model
{
    using System;

    public class BasicTariffStrategy : ITariffStrategy
    {
        private const double EuroPricePerYear = 5.0 * 12;

        private const double EuroPricePerkWh = 0.22;

        public double CalculateAnnualCost(double consumption)
        {
            if (consumption < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(consumption), "Argument can't be less than zero.");
            }

            return EuroPricePerYear + (consumption * EuroPricePerkWh);
        }
    }
}
