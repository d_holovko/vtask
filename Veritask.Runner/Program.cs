﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veritask.Model;

namespace Veritask.Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            var products = new ProductsList();

            products.AddProduct(new Product(ProductTariff.Packaged));
            products.AddProduct(new Product(ProductTariff.Basic));

            double[] cons = { 3500.0, 4500.0, 6000.0 };

            foreach (var consumption in cons)
            {
                int i = 0;
                Console.WriteLine($"\nIn case of {consumption}kWh/year consumption:");
                foreach (var product in products.GetSortedProducts(consumption))
                {
                    Console.WriteLine($"{++i}) {product.Tariff.ToString()} - {product.AnnualCost} euro.");
                }
            }
        }
    }
}
